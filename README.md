# Token Vision Animation: World Scope

This module sets 'Token Vision Animation' setting from client to world.
Now you can move player tokens safely without disclosure of scene data in the middle of movement.
